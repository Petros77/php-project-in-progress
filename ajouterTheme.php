<?php
include "/config/config.conf.php";

$themes = listerThemes(); 
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset=utf-8" />
		<title></title>
		<link type="text/css" rel="stylesheet" href="style.css" />
	</head>
	
	<body>
		<h1>Ajouter un theme</h1>
		<br/>
		<h3>list des themes stockes dans la base des donnees</h3>
		<table>
			<tr>
				<th>code</th>
				<th>designation</th>
			</tr>
			<?php
			for ($i = 0; $i < count($themes); $i++) {
				echo "<tr><td>" . $themes[$i]['id']  . "</td><td>" . $themes[$i]['designation'] . "</td></tr>";
			}
			?>
		</table>
		<br/><br/>
		<div class="container">
			<form id="ajouter" name="ajouter" method="POST" action="<?php echo WEB_ROOT ;?>AjouterThemeTraitement.php">
				<input type="hidden" id="ajouterSent" name="ajouterSent" value="1" />
				<?php
					if(isset($_GET["msg"]) && $_GET["msg"] == "err"){
						echo "<div class=\"err\">theme exciste deja ou ne peux pas etre vide<br/>";
					}?>
				Designation du theme : <input type="text" id="designation" name="designation" size="40" /><br/>
				<input type="submit" value="Ok" />
			</form>
		</div>
	
	</body>
</html>
	



