<?php
include "/config/config.conf.php";

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset=utf-8" />
		<title></title>
		<link type="text/css" rel="stylesheet" href="style.css" />
	</head>
	<body>
		<div class="header">
			<?php
				if(!isset($_SESSION['logged']) || $_SESSION['logged']['identifiant'] == '' || $_SESSION['logged']['identifiant'] == 0){ ?>
					<div class="container">
						<form id="login" name="login" method="POST" action="<?php echo WEB_ROOT ;?>loginTraitement.php">
							<input type="hidden" id="loginSent" name="loginSent" value="1" />
							<?php
							if(isset($_GET["msg"]) && $_GET["msg"] == "err"){
								echo "<div class=\"err\">Mauvais login ou password<br/>" ;
							}?>
							Login : <input type="text" id="nickname" name="nickname" size="20" /><br/>
							Password : <input type="password" id="password" name="password" size="20" /><br/>
							<input type="submit" value="Ok" />
						</form>
					</div>
				<?php }else{ ?>
						<div class="container">
							<?php echo $thisUser->nickname ; ?> - <a href="<?php echo WEB_ROOT ; ?>/login/logout.php">Logout</a>
						</div>
				<?php } ?>
		</div>
		
	</body>
</html>