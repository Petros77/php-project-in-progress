<?php


function PDOconnexionBD($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_NAME){
	global $DB_LINK_PDO;
	if($DB_LINK_PDO == null){
		try {
			$DB_LINK_PDO = new PDO('mysql:host='.$DB_HOST.';dbname='.$DB_NAME, $DB_USER, $DB_PASSWORD, array(PDO::ATTR_PERSISTENT => false));
			$DB_LINK_PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$DB_LINK_PDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
			$DB_LINK_PDO->exec('SET CHARACTER SET utf8') ;
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			return false;
		}
	}
	return true;
}


function listerThemes(){
	global $DB_LINK_PDO;

	$qry = "SELECT * FROM theme";
	$sth = $DB_LINK_PDO->prepare($qry);
	$sth->execute();
	$res= $sth->fetchAll(PDO::FETCH_ASSOC);
	$sth = null;
	if($res == false){
		return false ;
	}else{
		
		return $res ;
	}
}

function chercherTheme($designation) {
	global $DB_LINK_PDO;
	$qry = 'SELECT * FROM theme  WHERE designation=:designation';
	$sth = $DB_LINK_PDO->prepare($qry);
	$sth->bindValue(':designation', $designation);
	$sth->execute();
	$res = $sth->fetchAll(PDO::FETCH_ASSOC);
	$sth = null;
	if ($res != false && count($res) == 1) {
		return $res[0];
	}
	return false;
}

function ajouterTheme($designation) {
	global $DB_LINK_PDO;
	$qry = "INSERT INTO `qcm`.`theme` (`id`, `designation`) VALUES (NULL, 'paok')";
	$sth = $DB_LINK_PDO->prepare($qry);
	$sth->bindValue(':designation', $designation);
	$sth->execute();
	$sth = null;
}



function creeQuestion($texte, $themeId, $auteur) {
	
}
?>