<?php

class Question{
	public $idQuestion;
	public $texte;
	public $idTheme;
	public $profAuteur;
	
	function __construct() {
		$this->idQuestion = 0;
		$this->texte = '';
		$this->idTheme = 0;
		$this->profAuteur = null;
	}