<?php
class Prof{
	public $identifiant;
	public $password;
	public $name;
	public $e_mail;
	
	function __construct() {
		$this->identifiant = 0;
		$this->password = '';
		$this->name = '';
		$this->e_mail = '';
	}
	
	function login($identifiant, $password) {
		global $DB_LINK_PDO;
		$qry = 'SELECT * FROM professeur  WHERE identifiant=:identifiant AND password=:password';
		
		$sth = $DB_LINK_PDO->prepare($qry);
		$sth->bindValue(':identifiant', $identifiant);
		$sth->bindValue(':password', $password);
		//$sth->bindValue(':password', Prof::encryptPassword($password));
		
		$sth->execute();
		
		$res = $sth->fetchAll(PDO::FETCH_ASSOC);
		
		//var_dump($res);
		
		$sth = null;
		
		if ($res != false && count($res) == 1) {
			$row = array_pop($res);
			$prof = new Prof();
			$prof->identifiant = $row['identifiant'];
			$prof->password = $row['password'];
			$prof->name = $row['nom'];
			$prof->e_mail = $row['mail'];
			$_SESSION['logged']['identifiant'] = $prof->identifiant;
			$_SESSION['logged']['prof'] = serialize($prof);
			return true;
		}
		return false;
	}
	
	
	function logout() {
		
		session_unset();
		
	}
	
	public static function encryptPassword($password){
		
		return md5($password);
	}
	
	
}