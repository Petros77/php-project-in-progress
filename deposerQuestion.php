<?php
include "/config/config.conf.php";

$themes = listerThemes(); 
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset=utf-8" />
		<title></title>
		<link type="text/css" rel="stylesheet" href="style.css" />
	</head>
	
	<body>
		<h1>Deposer une question</h1>
		<br/>
		<h3>list des themes stockes dans la base des donnees</h3>
	
		<form action="deposerQuestionTraitement.php" method="POST">
			<input type="hidden" id="deposerSent" name="deposerSent" value="1" />
			<label>Choisir le theme de la question 
			<input list="browsers" name="theme" /></label>
			<datalist id="browsers">
				<?php
				for ($i = 0; $i < count($themes); $i++) {
					echo '<option value="' . $themes[$i]['designation'] . '">';
				}
				?>
			</datalist>
			<label for="texte">donnez moi la question</label>
			<input type="text" id="texte" name="texte" /><br/><br/>
			<label for="reponse01">donnez moi la premiere reponse</label>
			<input type="text" id="reponse01" name="reponse01" /><br/><br/>
			<label for="reponse02">donnez moi la deuxieme reponse</label>
			<input type="text" id="reponse02" name="reponse02" /><br/><br/>
			<label for="reponse03">donnez moi la troisieme reponse</label>
			<input type="text" id="reponse03" name="reponse03" /><br/><br/>
			 
			<div>
				quelle est la reponse correcte<br/>
				<input type="radio" id="genderChoice1" name="correct" value="1">
				<label for="genderChoice1">premier</label>
				<input type="radio" id="genderChoice2" name="correct" value="2">
				<label for="genderChoice2">deuxieme</label>
				<input type="radio" id="genderChoice3" name="correct" value="3">
				<label for="genderChoice3">troisieme</label>
			</div>
			<input type="submit" value="Ok" />
		</form>
	</body>
</html>